from platformutils.dynamodb_utils import DynamoDBUtils
from s3_dynamodb_parser_dal import ParseS3File
from platformutils.s3_utils import S3Utils
from decimal import *
import re
from io import BytesIO
from gzip import GzipFile
import csv
import json, ast

class Dashboard:

    def __init__(self, tname, region, level):
	self.final_dict = ['tripId', 'deviceId', 'distance', 'duration', 'endLat', 'endLong', 'hardAccels', 'os', 'startLat', 'startLong', 'status', 'suddenBrakes', 'transportMode', 'userId', 'endUtcDtime', 'insertTime', 'startUtcDtime', 'appVersion', 'appName', 'accelerationScore', 'brakingScore', 'speedingScore', 'endBattery', 'startBattery', 'tripScore', 'minutesSpeeding', 'phoneScore', 'insertLocalTime', 'startLocalTime', 'endLocalTime', 'tripStartReason', 'minutesPhone', 'modifiedBy', 'insertedBy', 'eventCount', 'modifyLocalTime', 'groupCode', 'extApp', 'reasonCode', 'modifiedLocalTime', 'modifiedTime', 'ModifyTimeN', 'osVersion', 'crashEvents', 'callEvents', 'uploadMode', 'sdkVersion', 'deviceModel', 'CS','processedTimeUtc']
	self.dict = ['tripid', 'deviceid', 'distance', 'duration', 'endlat', 'endlong', 'hardaccels', 'os', 'startlat', 'startlong', 'status', 'suddenbrakes', 'transportmode', 'userid', 'endutcdtime', 'inserttime', 'startutcdtime', 'appversion', 'appname', 'accelerationscore', 'brakingscore', 'speedingscore', 'endbattery', 'startbattery', 'tripscore', 'minutesspeeding', 'phonescore', 'insertlocaltime', 'startlocaltime', 'endlocaltime', 'tripstartreason', 'minutesphone', 'modifiedby', 'insertedby', 'eventcount', 'modifylocaltime', 'groupcode', 'extapp', 'reasoncode', 'modifiedlocaltime', 'modifiedtime', 'modifytimen', 'osversion', 'crashevents', 'callevents', 'uploadmode', 'sdkversion', 'devicemodel', 'cs','processedtimeutc']
	self.db = DynamoDBUtils(tname, region, level)
        self.parse_file = ParseS3File(region, level)
	self.s3 = S3Utils(region, level)

    def sdashboard(self, bucket, key):
	result = self.parse_file.get_dynamodb_file_data(bucket, key)
	bytestream = BytesIO(result)
	data = bytestream.read().decode('utf-8')
	for row in csv.DictReader(data.splitlines()):
	    h_data = self.parse_row(row)
	    finalData = self.handle_data(h_data)
	    formatDict = dict(zip(self.final_dict, finalData))
	    finalDict = dict((k, v) for k, v in formatDict.iteritems() if v)
	    response = self.db.insert_item_to_dynamo(finalDict)

    def dashboard(self, bucket, key):
	result = self.parse_file.get_dynamodb_file_data(bucket, key)
	dataread = str(result).split('\n')
	i = 0
    	while i<len(dataread):
            metadata = str(dataread[i]).split(',')
            i=i+1
	    finalData = self.handle_data(metadata)
	    formatDict = dict(zip(self.final_dict, finalData))
	    finalDict = dict((k, v) for k, v in formatDict.iteritems() if v)
	    if finalDict.get('deviceModel'):
	    	finalDict['deviceModel'] = finalDict.get('deviceModel').replace('#',',')
	    if finalDict:
		response = self.db.insert_item_to_dynamo(finalDict)	
	    
    def parse_row(self, line):
	"""
	This method is used to parse single row which is from S3 object.
        :param line: Provide line has to be parse.
        """
	r = json.dumps(line)
	line_value = json.loads(r)
	lst = []
	for k in self.dict:
	    if k in line_value and 'devicemodel' in k:
		line_value[k] = line_value[k].replace('#', ',')
		lst.append(line_value[k])
	    else:
		lst.append(line_value[k])
	return [item.encode('utf-8') for item in lst]

    def process_data(self, line):
	data = line.split(',')
	#finalData = self.handle_data(data)
	#formatDict = dict(zip(self.dict, finalData))
	#finalDict = dict((k, v) for k, v in formatDict.iteritems() if v)
	#finalDict['deviceModel'] = finalDict.get('deviceModel').replace('#',',')
	#response = self.db.insert_item_to_dynamo(finalDict)

    def handle_data(self, data):
	for i, x in enumerate(data):
    	    try:
            	data[i] = int(x)
    	    except ValueError:
		try:
		    data[i] = Decimal(x)
		except Exception:
		    pass
            	pass
	return data

    def get_s3_lst(self, bucket, prefix):
    	marker = None
    	count = 0
    	lst = []
    	while True:
	    data = self.s3.get_list_object(bucket, prefix, marker)
	    if data['IsTruncated']:
	    	for content in data.get('Contents'):
	    	    count = count + 1
     	    	    name = content['Key'].rsplit('/', 1)
	    	    marker = content['Key']
		    self.write_object(lst, name[1])
            else:
	    	if data.get('Contents'):
	    	    for content in data.get('Contents'):
	    	    	count = count + 1
     	    	    	name = content['Key'].rsplit('/', 1)
	    	    	marker = content['Key']
		    	self.write_object(lst, name[1])
	        break
    	return lst

    def write_object(self, lst, data):
    	if data != 'manifest' and data != '_SUCCESS':
	    lst.append(data)
