"""
S3 connection utils.

Author: Srinivas Rao Cheeti
email: scheeti@agero.com
Date: Mar 28, 2017

S3Utils class has the utils to copy, move, delete, read and write metadata, fetch keys, return the body of a desired s3
object. This could be used as a connector to s3 and do the required tasks on the buckets and objects.
"""

from cStringIO import StringIO

import boto3
import logging_helper as Log


class S3Utils():
    def __init__(self, r_name, level):
        self.logs = Log.log(level)
        self.logs.info('S3 Utils initialized')
        self.client = boto3.client('s3', region_name=r_name)

    def copy_object_to_destination(self, source_bucket, source_key, dest_bucket, dest_key):
        """This method copies s3 object from a source to destination.

        :param source_bucket: Provide the source bucket for the object to be copied.
        :param source_key: Provide the absolute path of the source key path (xyz/123.gz).
        :param dest_bucket: Provide the destination bucket name to where the object is to be copied.
        :param dest_key: Provide the absolute path of destination key name (xyz/123.gz).
        :param buffer_data: Concatenated string (buffer object).
        """
        self.logs.debug('Copied object - {:} from {:} to destination - {:} in this {:}'.format(source_key,source_bucket,dest_key,dest_bucket))
        self.client.copy_object(Bucket=dest_bucket, Key=dest_key, CopySource=source_bucket + '/' + source_key)

    def move_object_to_destination(self, source_bucket, source_key, dest_bucket, dest_key):
        """This method moves s3 object from a source to destination and deletes the object at in the source path.

        :param source_bucket: Provide the source bucket for the object to be moved.
        :param source_key: Provide the absolute path of the source key path (xyz/123.gz).
        :param dest_bucket: Provide the destination bucket name to where the object is to be moved.
        :param dest_key: Provide the absolute path of destination key name (xyz/123.gz).
        """
        self.logs.debug('Moved object - {:} from {:} to destination - {:} in this {:} and deleted in the destination'.format(source_key,source_bucket,dest_key,dest_bucket))
        self.client.copy_object(Bucket=dest_bucket, Key=dest_key, CopySource=source_bucket + '/' + source_key)
        self.client.delete_object(Bucket=source_bucket, Key=source_key)

    def delete_object(self, source_bucket, source_key):
        """This method deletes s3 object from the provided source path.

        :param source_bucket: Provide the source bucket for the object to be deleted.
        :param source_key: Provide the absolute path of the source key path (xyz/123.gz).
        """
        self.logs.debug('Deleted object - {:} from {:}'.format(source_key,source_bucket))
        self.client.delete_object(Bucket=source_bucket, Key=source_key)

    def object_file_size(self, source_bucket, source_key):
        """This method fetches the s3 object size in bytes.

        :param source_bucket: Provide the source bucket for the object to be deleted.
        :param source_key: Provide the absolute path of the source key path (xyz/123.gz).
        :return: object file size in bytes.
        """
        self.logs.debug('Fetched object size of {:} from {:} '.format(source_key,source_bucket))
        response = self.client.head_object(Bucket=source_bucket, Key=source_key)
        return response['ContentLength']

    def upload_object(self, filename, bucket, key_path):
        """This method uploads a local file or object to s3.

        :param filename: Provide the filename for the object to be uploaded ('/XYZ/.../.../123.gz').
        :param bucket: Provide the bucket for the object to be uploaded.
        :param key_path: Provide the absolute path of the source key path (xyz/123.gz) where the file has to be uploaded.
        """
        self.logs.debug('Uploaded this file {:} to this object {:} in bucket {:}'.format(filename,key_path,bucket))
        self.client.upload_file(Filename=filename, Bucket=bucket, Key=key_path)

    def download_object(self, filename, bucket, key_path):
        """This method downloads object form s3 to local path.

        :param filename: Provide the filename for the object to be downloaded to ('/XYZ/.../.../123.gz').
        :param bucket: Provide the bucket for the object to download.
        :param key_path: Provide the absolute path of the source key path (xyz/123.gz) for the object.
        """
        self.logs.debug('Downloaded this object {:} to this filepath {:} from bucket {:}'.format(key_path,filename,bucket))
        self.client.download_file(Bucket=bucket, Key=key_path, Filename=filename)

    def write_to_object(self, buffer_data, bucket, key_path):
        """This method writes a local buffer string object to s3 object.

        :param buffer_data: Provide the local buffer string to write to s3 (sample_string = 'Hi, how are you?').
        :param bucket: Provide the bucket where the object has to be written.
        :param key_path: Provide the absolute path of the key path (xyz/123.gz) for the object has to be written.
        """
        self.logs.debug('Wrote the buffer data to this key {:} in bucket {:}'.format(key_path,bucket))
        data_to_stream = StringIO(buffer_data)
        self.client.put_object(Bucket=bucket, Key=key_path, Body=data_to_stream.read())

    def get_object_content(self, bucket, key_path):
        """This method reads s3 objects contents and returns a buffer string.

        :param bucket: Provide the bucket where the object has to be read from.
        :param key_path: Provide the absolute path of the key path (xyz/123.gz) for the object to read its contents.
        :return Returns a String buffer of the file contents.
        """
        self.logs.debug('Fetched the content from key {:} in bucket {:}'.format(key_path,bucket))
        key_data = self.client.get_object(Bucket=bucket, Key=key_path)
        file_contents = key_data['Body'].read()
        return file_contents

    def get_prefix_keys(self, bucket, prefix):
        """This method returns all the keys in a prefix path of the key.

        :param bucket: Provide the bucket where the objects has to be read from.
        :param prefix: Provide the absolute path of the prefix to fetch all the subsequent keys (xyz/abc, xyz/abc/bbc etc).
        :return Returns a List of keys from the prefix path.
        """
        self.logs.debug('Fetching all the keys with this prefix {:} in bucket {:}'.format(prefix, bucket))
        items = self.client.list_objects(Bucket=bucket, Prefix=prefix)
        result_item = []
        for i in xrange(len(items['Contents'])):
            result_item.append(items['Contents'][i]['Key'])
        return result_item

    def get_prefix_keys_pagination(self, bucket, prefix):
        """This method returns all the keys in a prefix path if there are more than 999 objects. It paginates the path.

        :param bucket: Provide the bucket where the objects has to be read from.
        :param prefix: Provide the absolute path of the prefix to fetch all the subsequent keys (xyz/abc, xyz/abc/bbc etc).
        :return Returns a List of keys from the prefix path.
        """
        result_item = []
        paginator = self.client.get_paginator('list_objects')
        for result in paginator.paginate(Bucket=bucket, Delimiter='/', Prefix=prefix):
            for i in xrange(len(result['Contents'])):
                result_item.append(result['Contents'][i]['Key'])
        return result_item

    def upload_encrypted_object(self, bucket, key, enc_key, data_to_stream):
        """This method signs a file with AES256 and uploads the encrypted file.

        :param bucket: Provide the bucket where the objects has to be uploaded.
        :param key_path: Provide the absolute path of the key path (xyz/123.gz) where to upload the file.
        :param enc_key: Provide a 32 characted uuid as a encryption key with which to encrypt the file contents .
        :param data_to_stream: Provide a StringIO data string.
        Sample data:
        data = 'some string data'
        buffer_data = b64decode(data)
        data_to_stream = StringIO(data)
        """
        self.logs.debug('Uploading a encrypted file {:} to {:}'.format(bucket, key))
        self.client.put_object(Bucket=bucket, Key=key, Body=data_to_stream,
                               SSECustomerKey=enc_key,
                               SSECustomerAlgorithm='AES256')


    def get_encrypted_object(self, bucket, key, enc_key):
        """This method signs a file with AES256 and uploads the encrypted file.

        :param bucket: Provide the bucket where the objects has to be read from.
        :param key_path: Provide the absolute path of the key path (xyz/123.gz) where to read the file from.
        :param enc_key: Provide a 32 characted uuid as a encryption key with which to encrypt the file contents .
        :return Returns a String buffer of the file contents.
        """
        self.logs.debug('Uploading a encrypted file {:} to {:}'.format(bucket, key))
        response = self.client.get_object(Bucket=bucket, Key=key,
                             SSECustomerKey=enc_key,
                             SSECustomerAlgorithm='AES256')['Body'].read()

        return response

    def is_key_available(self, bucket, key_path):
        """This method find key object is available in S3.

        :param bucket: Provide the bucket where the objects has to be read from.
        :param key_path: Provide the absolute path of the key path (xyz/123.gz) where to read the file from.
        :return Returns the given key is available or not.
        """
	try:
            self.client.head_object(Bucket=bucket, Key=key_path)
	    return True
	except:
	    return False

    def get_list_object(self, bucket, prefix, marker=None):
        self.logs.debug('List the object from key {:} in bucket {:}'.format(prefix,bucket))
	if marker:
	    return self.client.list_objects(Bucket=bucket, Prefix = prefix, Marker=marker)
	else:
	    return self.client.list_objects(Bucket=bucket, Prefix = prefix)
