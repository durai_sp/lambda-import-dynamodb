"""
Time utils.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 10, 2017

TimeUtil class has the utils to get timestamp and get date with timedelta.
"""

from datetime import date, timedelta, datetime
import time


class TimeUtil:

    def totimestamp(self, dt, epoch=datetime(1970,1,1)):
	"""
	This method is used to convert datetime object into timestamp.
        :param dt: Provide datetime object has to be performed.
        :return Return UTC timestamp.
        """
    	td = dt - epoch
    	return (td.microseconds + (td.seconds + td.days * 86400) * 10**6) / 10**6 

    def get_date(self, delay_delta):
	"""
	This method is used to get current date with timedelta.
        :param delay_delta: Provide timedelta value.
        :return Return date.
        """
	yesterday = date.today() - timedelta(delay_delta)
	return yesterday

    def get_utc_timestamp(self, delay_delta, time_format=None):
	"""
	This method is used to get utc timestamp.
        :param delay_delta: Provide timedelta value.
	:param time_format: Provide time format which is milisecond, microsecond, default as second
        :return Return timestamp.
        """
	yesterday = self.get_date(delay_delta)
	data = str(yesterday.strftime('%b %d %Y')) + '  12:00AM'
    	datetime_object = datetime.strptime(data, '%b %d %Y %I:%M%p')
	if time_format == 'ms':
    	    return int(self.totimestamp(datetime_object)) * 1000
	else:
    	    return int(self.totimestamp(datetime_object))



